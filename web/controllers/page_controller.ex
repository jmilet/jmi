defmodule Jmi.PageController do
  use Jmi.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
