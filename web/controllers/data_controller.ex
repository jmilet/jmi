defmodule Jmi.DataController do
    use Jmi.Web, :controller
  
    def index(conn, _params) do
      ets_name = Application.get_env(:jmi, :ets_holder_name)
      value_list = Jmi.DataControlerLogic.list_ets_values(ets_name)
      json conn, %{data: value_list, "x-request-id": get_resp_header(conn, "x-request-id")}
    end
  end
  