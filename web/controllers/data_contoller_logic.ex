defmodule Jmi.DataControlerLogic do

  # Returns the content of the ets_name.
  def list_ets_values(ets_name) do
    :ets.tab2list(ets_name)
    |> Enum.flat_map(fn {key, value} ->
        %{Integer.to_string(key) => value}
      end)
    |> Enum.into(%{})
  end
end