# Jmi

This projects demoes some features of Erlang, Elixir and the Phoenix Web framework.

Basically it consists of a supervisor with three workers. Each worker creates a random
entry in an ETS table at a regular time basis. Also, each of them spawns a cleaning process
which will purge the entry some random seconds later "a la Redis expiration key".

Pointing to http://localhost:4000/api/test we can get the whole ETS status. As it is dynamic,
successsive calls will give different results.

```bash
juanmi@jmimac2 ~/jmi (master) $ http localhost:4000/api/test
HTTP/1.1 200 OK
cache-control: max-age=0, private, must-revalidate
content-length: 172
content-type: application/json; charset=utf-8
date: Sat, 20 Oct 2018 21:35:17 GMT
server: Cowboy
x-request-id: 2lfmtd0klpvtt2ifqk0018l1

{
    "data": {
        "11": 172008,
        "144": 49858,
        "1938": 186298,
        "2676": 97940,
        "358": 473496,
        "4680": 117797,
        "6254": 12328,
        "8787": 93380,
        "9785": 259402
    },
    "x-request-id": [
        "2lfmtd0klpvtt2ifqk0018l1"
    ]
}
```