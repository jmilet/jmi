defmodule Jmi do
  use Application

  import Supervisor.Spec

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do

    create_ets_random_values_table()
    create_ets_random_data()

    # Define workers and child supervisors to be supervised
    children = [
      # Start the endpoint when the application starts
      supervisor(Jmi.Endpoint, []),
      # Start your own worker by calling: Jmi.Worker.start_link(arg1, arg2, arg3)
      # worker(Jmi.Worker, [arg1, arg2, arg3]),
    ] ++ generate_random_value_producers(3)

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Jmi.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Jmi.Endpoint.config_change(changed, removed)
    :ok
  end

  # Creates the ETS that will hold the random values.
  defp create_ets_random_values_table() do
    ets_name = Jmi.EtsTables.random_table_name()
    :ets.new(ets_name, [:set, :named_table, :public])
  end

  # Create some random data to start width.
  defp create_ets_random_data() do
    ets_name = Jmi.EtsTables.random_table_name()
    delay_seconds = 10
    for _ <- 1..10, do: Jmi.ProducerLogic.generate_value_in_ets(self(), ets_name, delay_seconds)
  end

  # Generates up to n producers to be superviser.
  def generate_random_value_producers(n) do
    for i <- 1..n do
      worker_name = String.to_atom("random_value_producer_#{i}")
      worker(Jmi.Producer, [2, worker_name], id: worker_name)
    end
  end
end
