defmodule Jmi.ProducerLogic do

  @default_sleep_module Application.get_env(:jmi, :sleep_module)

  # Generate an entry in the given ETS and spawns the timeout and cleaning process
  # that will remove that same entry after a random amount of seconds.
  def generate_value_in_ets(process, ets_name, delay_seconds, sleep_module \\ nil) do
    sleep_module = sleep_module || @default_sleep_module

    key = create_entry(process, ets_name)
    spawn_cleaning_process(process, ets_name, key, sleep_module)
    schedule_next_run(process, delay_seconds)
  end

  # Create a random entry in the given ETS, both the key and the value are random integers.
  defp create_entry(process, ets_name) do
    key = :rand.uniform(10000)
    value = :rand.uniform(500000)
    :ets.insert(ets_name, {key, value})
    send(process, :inserted)
    key
  end

  # Spawns a cleaning process that purges the entry after a random number of seconds.
  defp spawn_cleaning_process(process, ets_name, key, sleep_module) do
    spawn(fn ->
      ttl_seconds = :rand.uniform(10) * 1000
      sleep_module.sleep(ttl_seconds)
      :ets.delete(ets_name, key)
      send(process, :deleted)
    end)
  end

  # Schedules the next run for the process.
  defp schedule_next_run(process, delay_seconds) do
    Process.send_after(process, :run, delay_seconds)
    send(process, :scheduled)
  end
end