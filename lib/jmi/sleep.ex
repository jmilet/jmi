defmodule Jmi.Sleep do
  @callback sleep(integer()) :: any
end

defmodule Jmi.SleepActual do
  @behaviour Jmi.Sleep

  def sleep(ms) do
    :timer.sleep(ms)
  end
end

defmodule Jmi.SleepNil do
  @behaviour Jmi.Sleep

  def sleep(_ms) do
  end
end