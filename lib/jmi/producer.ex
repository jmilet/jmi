defmodule Jmi.Producer do
  use GenServer

  def start_link(delay_seconds, name) do
    ets_name = Application.get_env(:jmi, :ets_holder_name)
    GenServer.start_link(__MODULE__, {delay_seconds * 1000, ets_name}, name: name)
  end

  ## Callbacks

  @impl true
  def init({delay_seconds, _} = state) do
    Process.send_after(self(), :run, delay_seconds)
    {:ok, state}
  end

  @impl true
  def handle_info(:run, {delay_seconds, ets_name} = state) do
    Jmi.ProducerLogic.generate_value_in_ets(self(), ets_name, delay_seconds)
    {:noreply, state}
  end

  @impl true
  def handle_info(_, state) do
    # IO.puts("default handle_info")
    {:noreply, state}
  end
end