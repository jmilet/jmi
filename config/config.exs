# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :jmi, Jmi.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "eB0kugRAGUVVacdwPcUfNi6V0Tp8Ouchp6yn4dhEf48ik31fmXIgjr1ESAL7G8o3",
  render_errors: [view: Jmi.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Jmi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# ETS name where to store the data.
config :jmi,
  ets_holder_name: :random_values,
  sleep_module: Jmi.SleepActual

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
