defmodule Jmi.DataControllerTest do
  use Jmi.ConnCase

  test "GET /api/test", %{conn: conn} do
    Application.put_env(:jmi, :sleep_module, Jmi.SleepNil)

    :ets.insert(Jmi.EtsTables.random_table_name(), {1000, 2000})

    conn = get conn, "/api/test"
    response = json_response(conn, 200)
    assert %{"data" => %{"1000" => 2000}, "x-request-id" => [_]} = response
  end
end

