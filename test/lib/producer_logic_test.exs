defmodule Jmi.ProducerLogicTest do
  use ExUnit.Case, async: false

  setup do
    # Cleans the ETS.
    ets_name = :ets.new(:test_producer_logic, [:public, :set, :named_table])
    :ets.delete_all_objects(ets_name)
    :ok  
  end

  test "it generates and expires a key in the ETS" do
    ets_name = :test_producer_logic
    delay_seconds = 0
    Jmi.ProducerLogic.generate_value_in_ets(self(), ets_name, delay_seconds, Jmi.SleepNil)

    assert_receive :inserted, 500
    assert [{k, v}] = :ets.tab2list(ets_name)

    assert_receive :deleted, 500
    assert [] = :ets.tab2list(ets_name)

    assert_receive :run, 500
  end
end